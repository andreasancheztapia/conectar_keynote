Este repositório contém os slides para a keynote no evento ConectaR sobre o Observatório COVID19-BR.

A palestra será transmitida dia 4 de fevereiro de 2021 (link TBD), seguida de uma sessão de perguntas ao vivo.

Os slides podem ser vistos aqui: https://andreasancheztapia.gitlab.io/conectar_keynote

Os slides podem ser editados diretamente neste arquivo: https://gitlab.com/andreasancheztapia/conectar_keynote/-/blob/master/conectaR_keynote.Rmd (clique o botão azul EDIT)

Quando terminar a edição, vá para o final da página, edite (opcional) a caixa Commit message e clique o botão verde, Commit changes. Os slides serão atualizados automaticamente por GitLab.
